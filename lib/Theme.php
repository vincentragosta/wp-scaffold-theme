<?php

namespace Theme;

use Gilgamesh\Service\RegisterExtensionService;
use Theme\Options;
use Theme\Support\TemplateWrapper;

/**
 * Class Theme
 * @package Theme
 * @author  Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class Theme
{
    const API_PREFIX = 'api';
    const NAV_MENUS = [
        'primary_navigation' => 'Primary/Header Navigation',
        'footer_navigation' => 'Footer Navigation'
    ];
    const THEME_SUPPORT = [
        'soil-clean-up',
        'soil-disable-asset-versioning',
        'soil-disable-trackbacks',
        'soil-js-to-footer',
        'soil-nav-walker',
        'soil-nice-search',
        'soil-relative-urls',
        'title-tag',
        'post-thumbnails'
    ];
    const EXTENSIONS = [
        Options\ThemeSettings::class
    ];

    public function __construct()
    {
        TemplateWrapper::init();
        add_action('after_setup_theme', [$this, 'setup']);
        add_action('wp_enqueue_scripts', [$this, 'assets'], 100);
        add_action('enqueue_block_editor_assets', [$this, 'blockEditorAssets']);
        add_action('wp_body_open', [$this, 'afterOpeningBody']);
    }

    public function setup()
    {
        add_filter('show_admin_bar', '__return_false');
        add_filter('body_class', function ($classes) {
            $home_id_class = 'page-id-' . get_option('page_on_front');
            $remove_classes = [
                'page-template-default',
                $home_id_class
            ];
            $classes = array_diff($classes, $remove_classes);
            return $classes;
        });
        add_filter('rest_url_prefix', function () {
            return static::API_PREFIX;
        });

        register_nav_menus(static::NAV_MENUS);
        array_map('add_theme_support', static::THEME_SUPPORT);

        (new RegisterExtensionService(static::EXTENSIONS))->run();
    }

    public function assets()
    {
        wp_enqueue_script('theme/scripts', get_template_directory_uri() . '/build/frontend.js', null, true, true);
        wp_enqueue_style('theme/styles', get_template_directory_uri() . '/build/frontend.css', null, true);
        $js_vars = apply_filters('global_js_vars', [
            'ajax_url' => admin_url('admin-ajax.php'),
            'api_url' => trailingslashit(home_url(rest_get_url_prefix()))
        ]);
        wp_localize_script('theme/js', 'wp_scaffold', $js_vars);
    }

    public function blockEditorAssets()
    {
        wp_enqueue_script('theme/editor-scripts',
            get_stylesheet_directory_uri() . '/build/backend.js',
            ['wp-i18n', 'wp-element', 'wp-blocks', 'wp-components', 'wp-editor'],
            filemtime(get_stylesheet_directory_uri() . '/build/backend.js'),
            true
        );
        wp_enqueue_style(
            'theme/editor-styles',
            get_stylesheet_directory_uri() . '/build/backend.css',
            ['wp-edit-blocks'],
            filemtime(get_stylesheet_directory_uri() . '/build/backend.css')
        );
    }

    public function afterOpeningBody()
    {
        $sprite = get_theme_file_path('build/sprite.svg');
        if (file_exists($sprite)) {
            echo sprintf('<div class="svg-sprite">%s</div>', file_get_contents($sprite));
        }
    }
}
