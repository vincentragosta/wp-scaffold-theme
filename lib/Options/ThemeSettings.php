<?php

namespace Theme\Options;

use Gilgamesh\Options\OptionsPage;

/**
 * Class ThemeSettings
 * @package Theme\Options
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $option_name
 * @property string $parent
 * @property string $page_title
 */
final class ThemeSettings extends OptionsPage
{
    protected $option_name = 'theme_settings';
    protected $page_title = 'Theme Settings';

    public function __construct()
    {
        add_action("fm_submenu_{$this->getOptionName()}", function () {
            $fm = new \Fieldmanager_Group([
                'name' => $this->getOptionName(),
                'tabbed' => 'vertical',
                'children' => [
                    'general' => new \Fieldmanager_Group([
                        'label' => 'General',
                        'children' => [
                            'logo' => new \Fieldmanager_Media('Logo'),
                            'social' => new \Fieldmanager_Group([
                                'label' => 'Social Icons',
                                'limit' => 0,
                                'add_more_label' => 'Add Social Icon',
                                'children' => [
                                    'icon' => new \Fieldmanager_Select('Icon', ['options' => ['Facebook', 'Twitter', 'Instagram', 'LinkedIn']]),
                                    'url' => new \Fieldmanager_Link('Url')
                                ]
                            ])
                        ]
                    ])
                ]
            ]);
            $fm->activate_submenu_page();
        });
    }
}
