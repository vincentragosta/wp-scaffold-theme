<?php if (have_posts()): ?>
    <?php while (have_posts()): the_post(); global $post; ?>
        <article <?php post_class(); ?>>
            <?php if (is_singular()): ?>
                <div class="container">
                    <h1 class="heading heading--xlarge"><?php the_title() ?></h1>
                    <?php if (!empty(get_the_content())): ?>
                        <div class="entry-content">
                            <?php the_content(); ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php else: ?>
                <h2 class="heading heading--medium"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
                <div class="entry-summary">
                    <?php the_excerpt(); ?>
                </div>
            <?php endif; ?>
        </article>
    <?php endwhile; ?>
<?php else: ?>
    <div class="container">
        <div class="alert alert-warning">
            <?php _e(is_404() ?
                'Sorry, but the page you were trying to view does not exist.' :
                'Sorry, no results were found.', 'wp-scaffold');
            ?>
        </div>
    </div>
<?php endif; ?>
<?php the_posts_navigation(); ?>
