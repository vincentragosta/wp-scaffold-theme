const defaultConfig = require('@wordpress/scripts/config/webpack.config');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const path = require('path');

module.exports = {
    ...defaultConfig,
    mode: 'development',
    entry: {
        frontend: [
            path.resolve(__dirname, '../../plugins/ishtar/assets', 'frontend.js'),
            path.resolve(__dirname, '../../plugins/ereshkigal/assets', 'frontend.js'),
            path.resolve(__dirname, 'assets/', 'frontend.js'),
        ],
        backend: [
            path.resolve(__dirname, '../../plugins/ishtar/assets', 'backend.js'),
            path.resolve(__dirname, '../../plugins/ereshkigal/assets', 'backend.js'),
            path.resolve(__dirname, 'assets/', 'backend.js')
        ]
    },
    module: {
        ...defaultConfig.module,
        rules: [
            ...defaultConfig.module.rules,
            {
                test: /assets\/images\/svg\/.*\.svg$/,
                loader: 'svg-sprite-loader',
                options: {
                    extract: true,
                    spriteFilename: './sprite.svg',
                    runtimeCompat: true
                }
            }
        ],
    },
    plugins: [
        ...defaultConfig.plugins,
        new SpriteLoaderPlugin({
            plainSprite: true
        })
    ],
};
